﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    public class PlayerStats
    {
        public float Health = 100;
    }

    public PlayerStats playerStats = new PlayerStats();

    public void DamagePlayer (int damage) {
        playerStats.Health -= damage;
        if (playerStats.Health <= 0) {
            Debug.Log("KILL PLAYER");
        }
    }

}
